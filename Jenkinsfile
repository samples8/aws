pipeline {
  agent none
  // Setup for aws access for terraform
  // Set this variables in Jenkins credentials.
  // Alternatively you can mount secrets volume into terraform container to ~/.aws/credentials
  environment {
    AWS_ACCESS_KEY_ID     = credentials('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = credentials('AWS_SECRET_ACCESS_KEY')
  }
  parameters {
    string(name: "TERRAFORM_ROOT", defaultValue: "terraform/", description: "Relative path to terraform root directory in the repo")
  }
  
  stages {
        stage('Choice') {
            input {
                message "Cloud provider" 
                ok "Yes"
                // This is not setting groovy params.CLOUDPROVIDER but CLOUDPROVIER environment variable
                // TODO: Check global level like with example here: https://gist.github.com/andymotta/5484f1d2a8f6d8eb3b18f5d415ef3de5
                parameters {
                   choice( choices: ['AWS' , 'Azure' ,'GCP'],
                           description: 'Which Cloud to deploy to',
                           name: 'CLOUDPROVIDER')
                }
            }
            when {
                  expression { env.CLOUDPROVIDER == 'AWS'}
            }
            // just testing condtional here (do something extra for AWS)
            steps {    
                script {
                  echo "Your choice was [${env.CLOUDPROVIDER}] [${CLOUDPROVIDER}]"
                  // Branch name envrionment mapping definition
                  def branch2env = ['develop': 'dev', 'master':'prod']
                  env.DEPLOY_ENV = branch2env[env.BRANCH_NAME]
                  //TODO: CLOUDPROVIDER parameters is not passed to other stages
                  //@see https://jenkins.io/doc/book/pipeline/syntax/#input
                  //[..] Any parameters provided as part of the input submission will be available in the environment for the rest of the stage.[..]
                  env.CLOUD = env.CLOUDPROVIDER
                  echo "Applying changes for [${DEPLOY_ENV}] Environment"
                  env.TERRAFORM_ROOT = params.TERRAFORM_ROOT + "environments/" + env.DEPLOY_ENV
                }
            }
        }
        stage('Setup Infra with terrform'){
          // We need this setup here as hashicorp/terraform:light runs by default as a root
          agent {
                dockerfile {
                  //Use docker file from this location. It could define general tooling image
                  dir 'docker-images/lib/terraform'
                  //Run on linux nodes
                  label 'Linux'
                  //pickup jenkins user parameters to burn it into image
                  additionalBuildArgs '--build-arg JENKINS_UID=$(id -u) --build-arg JENKINS_GID=$(id -g)'
                  //Setting access to terraform files in the workspace to terraform
                  args '-w /terraform -v ${WORKSPACE}:/terraform' 
                }
          }
          stages {
            stage ('Version') {
              steps  {
                sh 'set'
                sh "cd ${TERRAFORM_ROOT} ; terraform --version"
              }
            }
            stage ('Validate') {
              steps  {
                sh "cd ${TERRAFORM_ROOT} ; terraform init"
                sh "cd ${TERRAFORM_ROOT} ; terraform validate"
              }
            }
            stage ('Plan') {
              steps  {
                echo "Adding ssh key if not exits"
                //This key path is default used by terraform code
                sh "[ -f ~/.ssh/id_rsa.pub ] || ssh-keygen -q -N \"\" -f ~/.ssh/id_rsa"
                echo  "Deploying to [${CLOUD}] cloud provider and environment ${DEPLOY_ENV}"
                sh "cd ${TERRAFORM_ROOT} ; " + 'terraform plan -out=terraform.plan -var "env=${DEPLOY_ENV}" -var "cloud=${CLOUD}" -var "aws_access_key=${AWS_ACCESS_KEY_ID}" -var "aws_secret_key=${AWS_SECRET_ACCESS_KEY}"'
              }
            }
            stage('Apply Terraform?') {
              input {
                message "Review terraform plan result, Apply?" 
                ok "Yes"
                parameters {
                   choice( choices: ['YES' , 'NO'],
                           description: 'Terraform action to apply',
                           name: 'TERRAFORMACTION')
                }
              }
              when {
                  expression { env.TERRAFORMACTION == 'YES'}
              }
              steps {
                echo  "Your choice was [${TERRAFORMACTION}]"
                echo  "Deploying to [${CLOUDPROVIDER}] cloud provider"
                //sh 'terraform apply -auto-approve terraform.plan'
              }
            }
          }
        }
  }
}
  