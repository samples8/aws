#!/bin/bash
# Uses terrafom docker image to run terraform command

# Image to be used to 
# Oryginal hashicorp image
# IMAGE=hashicorp/terraform:light
IMAGE="rst/terraform"
set -e
TERRCMD="docker run --rm -w /terraform -v `pwd`:/terraform --rm ${IMAGE}"
${TERRCMD} $@
