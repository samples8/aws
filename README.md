# AWS automation samples
This is repo to present CI/CD samples
# Folder structure
```bash
├── docker-images/   # docker images required to support deployment from CI/CD 
├── k8s/             # K8s samples
├── samples/         # Sample files
├── scripts/         # Scripts supporting deployment process 
├── terraform/       # Terraform files for deployment
├── Jenkinsfile      # Jenkins file supporting terraform deployment
├── README.md        # This file
├── SETUP-README.md  # Extra setup instructions

```
## Jenkins pipeline
Jenkinsfile here is a little play with Ci/CD and interactions in UI
underlying terraform file is for simple aws test only.

This Jenkins file works with Cloudbees Jenkins distribution.

Assumptions:
* AWS secrets has been set in Jenkins
* Jenkins slave with 'Linux' label has installed docker engine and able to run docker images
* Jenkins slave has default registry configure to access hashicorp/terraform:light docker image
  
