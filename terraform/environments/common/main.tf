locals { 
    # Common configuration for all envrionments
  config = {
        tags = {
            "environment" = lower(var.env)
            "creator"     = var.creator
    }
  }
}

# Abstract module to select provider for resource creation

module "aws_network" {
    source  = "../../modules/aws/network"

    enabled = ( lower(var.cloud) == "aws" ? true : false )
    # We should pass here i.e. subnet ranges
    config  = local.config
}
# TODO: Implement network for azure
#
# module "azure_network" {
#    source  = "../modules/azure/network"
#
#    enabled = var.config.cloud == "azure" ? true : false
#    config  = var.config
#}

