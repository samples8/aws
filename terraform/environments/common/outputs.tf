# Common configuration for all environments
output "config" {
    value = merge (
        module.aws_network.config,
        # module.azure_network.config,
        {
        cloud = lower(var.cloud)
        # Add more tags here which would be applied to all resources
        tags = local.config.tags
    }
    )
}