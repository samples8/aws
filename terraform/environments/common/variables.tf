
# Default configuration for all environments, subject to parametrisation
variable "cloud" {
  default = "AWS"
}
# Envrionment code name, it has to be alligned with directory names
# case insensitive
variable "env" {
  default = "DEV"
}
# Who created resource
variable "creator" {
  default = "terraform"
}
