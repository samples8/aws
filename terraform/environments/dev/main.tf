# If you do not have terraform installed where this code is run, but you have a docker:
# TERRCMD="docker run --env AWS_ACCESS_KEY_ID=<KEY> --env AWS_SECRET_ACCESS_KEY=<SECRET>  --rm -t -i -w /terraform -v `pwd`:/terraform hashicorp/terraform:light"
# see building k8s with terraform https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform/

# peform common actions and configuration for all modules
# parameters
#   cloud - cloud code name which is reflected in directory structure. i.e. 'aws' or 'azure'
#         - cloud code name is case insensitvie
module "common" {
    source = "../common"
    cloud  = var.cloud
    env = var.env
}
# Create machine based on common configuration and provider selection
# cloud  - The cloud selection - passing through config breaks due to plan failure on elivs expression evaluation
# amount - The amount of instances to create
module "vm" {
   source = "../../modules/vm"
   config = module.common.config
   cloud  = var.cloud
   amount = 2
}
# This is just to present possiblity of different providers usage.
# I would create sepparate enviornments for different clouds for simplicity 
module "vm_azure" {
   source = "../../modules/vm"
   config = module.common.config
   cloud  = "azure"
   amount = 1
}
output "common_config" {
  value = module.common.config
}
# Debug to see result of machine creation
# Shows instance creation parameters
output "vm_config" {
 value = module.vm.config
}
