variable "aws_access_key" {
  default = ""
}
variable "aws_secret_key" {
  default = ""
}
variable "cloud" {
  default = "AWS"
}
# Envrionment selection
variable "env" {
  default = "DEV"
}

variable "deployer-pub-key-path" {
  default = "~/.ssh/id_rsa.pub"
}
variable "deployer-priv-key-path" {
  default = "~/.ssh/id_rsa"
}