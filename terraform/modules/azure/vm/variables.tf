# List of  instances names to be created. See AWS implementation
variable "host_names" {
  default = []
  type = list(string)
}
# This is mandatory to be passed with network configuration data
variable "config" {
}
