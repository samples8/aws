# Abstract module to select provider for resource creation
locals {
    aw =  (lower(var.cloud) == "aws") ? 1 : 0
    az =  (lower(var.cloud) == "azure") ? 1 : 0
}
module "aws_vm" {
    source  = "../aws/vm"
    # host_names is equivalent to using count in the resources
    # We cannot you conditional expresion during plan (terraform v.0.12.24) as terraform cannot evaluate elvis operator
    # Resources generation is based in list size which could be generated during plan phase
    host_names = [ for v in range (1, local.aw*var.amount+1 ): format("%s%02d",var.hostname_prefix, v)]
    config  = var.config
}

module "azure_vm" {
    source  = "../azure/vm"

    host_names = [ for v in range (1,  local.az*var.amount+1 ): format("%s%02d",var.hostname_prefix, v)]
    config  = var.config
}

