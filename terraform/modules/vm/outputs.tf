# Resulted machine configuration

# Assumes here that if modules has not been activated then module produces empty map of variables
output "config" {
    value = {
        aws   = "${ merge( module.aws_vm.config, module.azure_vm.config ) }"
    }
}