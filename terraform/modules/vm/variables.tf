
# Default configuration for all environments, subject to parametrisation
variable "config" {
  default =  {
     cloud = "aws" # Environment selection
  }
}
variable "hostname_prefix" {
  description = "Hostname prefix used for generating name of hostname if more hosts are created"
  default = "VM-"
}
variable "amount" {
  description = "How many machines is to be created on given parameters"
  default = 1
}
variable "cloud" {
    description = "Mandatory required to select cloud type, cant be passed"
}