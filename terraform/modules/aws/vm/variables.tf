variable "aws_access_key" {
  default = ""
}
variable "aws_secret_key" {
  default = ""
}
variable "deployer-pub-key-path" {
  default = "~/.ssh/id_rsa.pub"
}
variable "deployer-priv-key-path" {
  default = "~/.ssh/id_rsa"
}
# List of  instances names to be created. See AWS implementation
variable "host_names" {
  default = []
  type = list(string)
}
# This is mandatory to be passed with network configuration data
variable "config" {
}
