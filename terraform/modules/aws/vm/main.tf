# Common tags for all resources
# Extending here tags list as required in the context of the resource (VM here)
locals {
  tags = merge ( 
    var.config.tags, 
    {
      resource_type = "vm"
    } 
  )
}
# SSH access to instance
resource "aws_security_group" "ssh" {
  count = length(var.host_names) >  0 ? 1  : 0
  vpc_id = var.config.vpc.id
  name = "sgSSH"

  ingress {
    description = "Allow SSH to instance"
    protocol  = "tcp"
    self      = false
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = local.tags
}
# HTTP access to instance

resource "aws_security_group" "squid" {
  count = length(var.host_names) >  0 ? 1  : 0
  vpc_id = var.config.vpc.id
  name = "sgSQUID"

  ingress {
    description = "Allow on port 3128"
    protocol  = "tcp"
    self      = false
    from_port = 3128
    to_port   = 3128
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = local.tags
}

# aws ec2 describe-images --image-ids ami-0cb790308f7591fa6 
# General purpose image description 
# aws ec2 describe-images --image-ids ami-0cb790308f7591fa6
# {
#     "Images": [
#         {
#             "VirtualizationType": "hvm", 
#             "Description": "Amazon Linux 2 AMI 2.0.20200304.0 x86_64 HVM gp2", 
#             "Hypervisor": "xen", 
#             "ImageOwnerAlias": "amazon", 
#             "EnaSupport": true, 
#             "SriovNetSupport": "simple", 
#             "ImageId": "ami-0cb790308f7591fa6", 
#             "State": "available", 
#             "BlockDeviceMappings": [
#                 {
#                     "DeviceName": "/dev/xvda", 
#                     "Ebs": {
#                         "SnapshotId": "snap-0ac626f9cef85a65b", 
#                         "DeleteOnTermination": true, 
#                         "VolumeType": "gp2", 
#                         "VolumeSize": 8, 
#                         "Encrypted": false
#                     }
#                 }
#             ], 
#             "Architecture": "x86_64", 
#             "ImageLocation": "amazon/amzn2-ami-hvm-2.0.20200304.0-x86_64-gp2", 
#             "RootDeviceType": "ebs", 
#             "OwnerId": "137112412989", 
#             "RootDeviceName": "/dev/xvda", 
#             "CreationDate": "2020-03-07T21:46:41.000Z", 
#             "Public": true, 
#             "ImageType": "machine", 
#             "Name": "amzn2-ami-hvm-2.0.20200304.0-x86_64-gp2"
#         }
#     ]
# }

# ami-0cb790308f7591fa6 - Amazon Linux 2 image
# resource "aws_instance" "web" {
#   ami           = "ami-0cb790308f7591fa6"
#   instance_type = "t2.micro"
# }

# to find Linux AMI use:
# aws ec2 describe-images  --filters "Name=description,Values=*Linux 2*gp2" --owners amazon  --query 'Images[*].[ImageId, CreationDate, Name, Description]' --output text
# to get most fresh image from the list use:
# aws ec2 describe-images  --filters "Name=description,Values=*Linux 2*gp2" --owners amazon  --query 'sort_by(Images,&CreationDate)[-1].[ImageId, CreationDate, Name, Description]' --output text

# https://www.terraform.io/docs/providers/aws/d/ami.html

# This grabs ami which we want to run
data "aws_ami" "image" {
  count = length(var.host_names) >  0 ? 1  : 0 
  most_recent      = true
  owners           = ["amazon"]

  filter {
    name   = "description"
    values = ["*Linux 2 AMI*gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }  

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
# Instance definition with bootstrap code
resource "aws_key_pair" "deployer" {
  count = length(var.host_names) >  0 ? 1  : 0
  key_name   = "terraform-deployer-key"
  public_key = file(var.deployer-pub-key-path)
}

resource "aws_instance" "host"{
  for_each = toset(var.host_names)

  ami = data.aws_ami.image[0].id
  
  instance_type = "t2.micro"
  subnet_id = var.config.subnets.main.id          # 
  associate_public_ip_address = true
  vpc_security_group_ids = [ 
    aws_security_group.ssh[0].id, 
    aws_security_group.squid[0].id
  ] 
  key_name = "terraform-deployer-key"

  user_data = <<-EOF
  #!/bin/bash
  yum update -y
  yum install -y squid
  systemctl start squid
  systemctl enable squid
  EOF
  tags = merge ( local.tags,
  {
    vmname  = each.key
  }
  )
  # Provisioners block is a bad pratice, this should be done via cloud-init/user-data and bootstraping i.e. via ansible
  provisioner "local-exec" {
    command = "echo \"acl localhost src $(curl ifconfig.co)/32 # access from machine where terraform runs\" > /tmp/squid.acl"
  }
  provisioner "file" {
    source = "/tmp/squid.acl"
    destination = "/tmp/squid.acl"
    connection {
      type  = "ssh"
      user  = "ec2-user"
      private_key = file(var.deployer-priv-key-path)
      host = self.public_ip
    }
  }
  provisioner "remote-exec" {
    inline = [
      "i=0 ; while [ ! -f /etc/squid/squid.conf ] && [ $i -lt 60 ]; do sleep 1; let i=$i+1; echo .; done; sudo grep \"$(cat /tmp/squid.acl)\" /etc/squid/squid.conf || ( sudo sh -c 'cat /tmp/squid.acl >> /etc/squid/squid.conf' && sudo service squid restart ) || exit 0"  
    ]
    connection {
      type  = "ssh"
      user  = "ec2-user"
      private_key = file(var.deployer-priv-key-path)
      host = self.public_ip
    }
  }
}