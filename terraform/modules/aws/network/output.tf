output "config" {
    value = {
       subnets =  { 
           "main" = aws_subnet.main
       } 
       vpc = aws_vpc.main
    }
}