locals {
  tags = merge ( 
    var.config.tags, 
    {
      resource_type = "network"
    } 
  )
}

//See also aws_default_vpc https://www.terraform.io/docs/providers/aws/r/default_vpc.html
# TODO: Externalize IP ranges to common file
resource "aws_vpc" "main" {
  cidr_block = "10.1.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = local.tags

}
# Required to setup internet connectivity
resource "aws_internet_gateway" "ig" {
    vpc_id = aws_vpc.main.id
    tags = local.tags
}
# Getting default VPC route table under management
resource "aws_default_route_table" "r" {
  default_route_table_id = aws_vpc.main.default_route_table_id

  tags = {
    Name = "Default table"
  }
}
# Adding default route to internet for all subnets in VPC
resource "aws_route" "igw" {
  route_table_id = aws_default_route_table.r.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id  = aws_internet_gateway.ig.id
  
}
# main VPC subnet where instance will live
resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.1.0.0/24"
  tags = local.tags
}

# Setting setup to instance
resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.main.id

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = local.tags
}