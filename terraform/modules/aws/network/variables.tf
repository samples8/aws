variable "aws_access_key" {
  default = ""
}
variable "aws_secret_key" {
  default = ""
}
# Mandatory to be passed from common module
variable "config" {
}
# Is module enable to produce and return resorces in config object?
# TODO: Implement count base switches on resources.
variable "enabled" {
  default = "false"
}
